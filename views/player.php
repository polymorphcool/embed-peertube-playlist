<?php if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly ?>

<?php
$EMBED = '/videos/embed/';
$EXTERNAL = '/videos/watch/playlist/';
$PLAYER_PARAMS = '?controls=1&api=1&title=0&autoplay=1&fit=H';
?>

<script>
	jQuery(document).ready(function(){
		jQuery('#playlist_peertube_grid_<?= $playlist->id ?> .video .video_container').click(function(){
			//supprime la vidéo déjà en cours de lecture
			jQuery('#playlist_peertube_grid_<?= $playlist->id ?> .video .video_container iframe').remove();
			//affiche le lecteur youtube
			jQuery(this).append('<iframe width="100%" height="100%" src="'+jQuery(this).attr('rel')+'?autoplay=1&rel=0" frameborder="0" allowfullscreen></iframe>');
		});
	});

<?php
$peertube_playlist = "var peertube_playlist={";
$peertube_count = 0;
foreach($data->data as $video)
{
	if ($peertube_count != 0) {
		$peertube_playlist .= ",";
	} else {
		$peertube_playlist .= "'".$playlist->id."' : {";
		$peertube_playlist .= "current : 0,";
		$peertube_playlist .= "list : [";
	}
	$peertube_playlist .= "{";
	$peertube_playlist .= "title:'".str_replace("'", "\'", $video->video->name )."',";
	$peertube_playlist .= "preview:'".$peertube_url.$video->video->previewPath."',";
	$peertube_playlist .= "embed:'".$peertube_url.$video->video->embedPath."',";
	$peertube_playlist .= "uuid:'".$video->video->uuid."'";
	$peertube_playlist .= "}";
	$peertube_count += 1;
}
$peertube_playlist .= "]}};";
echo $peertube_playlist;
?>
function playlist_peertube_load( playlist_id ) {
	var pl = peertube_playlist[ playlist_id ];
	var media = pl.list[pl.current];
	var url = "'<?= $peertube_url.$EMBED ?>'" + media.uuid + "'<?= $PLAYER_PARAMS ?>'";
	document.getElementById( 'playlist_peertube_playerframe_' + playlist_id ).src = "<?= $peertube_url.$EMBED ?>" + media.uuid + "<?= $PLAYER_PARAMS ?>";
	// rendering list
	for ( var i  = 0; i < pl.list.length; ++i ) {
		var el = document.getElementById( 'playlist_peertube_li' + i );
		el.className = 'playlist_peertube_li';
		if ( i == pl.current ) {
			el.className += ' selected';
		}
	}
}
function playlist_peertube_next( playlist_id ) {
	var pl = peertube_playlist[ playlist_id ];
	pl.current += 1;
	if ( pl.current >= pl.list.length ) { pl.current = 0; }
	playlist_peertube_load( playlist_id );
	return false;
}
function playlist_peertube_previous( playlist_id ) {
	var pl = peertube_playlist[ playlist_id ];
	pl.current -= 1;
	if ( pl.current < 0 ) { pl.current = pl.list.length-1; }
	playlist_peertube_load( playlist_id );
	return false;
}
function playlist_peertube_previous( playlist_id ) {
	var pl = peertube_playlist[ playlist_id ];
	pl.current -= 1;
	if ( pl.current < 0 ) { pl.current = pl.list.length-1; }
	playlist_peertube_load( playlist_id );
	return false;
}
function playlist_peertube_jumpto( playlist_id, pos ) {
	var pl = peertube_playlist[ playlist_id ];
	if (pos < 0) { pos = 0; }
	if (pos >= pl.list.length) { pos = pl.list.length-1; }
	pl.current = pos;
	playlist_peertube_load( playlist_id );
	return false;
}
</script>
<div class="playlist_peertube_player" id="playlist_peertube_player_<?= $playlist->id ?>">
<div class="playlist_peertube_menu">
	<h3><a href="<?= $peertube_url.$EXTERNAL.$playlist->playlist_id ?>">playlist</a></h3>
	<ul class="playlist_peertube_videos">
<?php
$peertube_count = 0;
foreach($data->data as $video)
{
	$class = "playlist_peertube_li";
	if ( $peertube_count == 0 ) {
		$class .= " selected";
	}
	echo '<li id="playlist_peertube_li'.$peertube_count.'" class="'.$class.'"><a id="playlist_peertube_item'.$peertube_count.'" href="#" onclick="playlist_peertube_jumpto('."'".$playlist->id."',".$peertube_count.');">'.$video->video->name.'</a>';
	echo '<span class="video_description">'.$video->video->description.'</span>';
	echo '</li>';
	$peertube_count += 1;
}
?>
	</ul>
	<a class="playlist_peertube_previous" href="#" onclick="playlist_peertube_previous('<?= $playlist->id ?>');">previous</a>
	<a class="playlist_peertube_next" href="#" onclick="playlist_peertube_next('<?= $playlist->id ?>');">next</a>
</div>
<div class="playlist_peertube_video" id="playlist_peertube_playervideo_<?= $playlist->id ?>">
	
<?php
foreach($data->data as $video)
{
	echo '<h3>'.$video->video->name.'</h3>';
	echo '<iframe id="playlist_peertube_playerframe_'.$playlist->id.'" width="100%" height="100%" src="'.$peertube_url.$EMBED.$video->video->uuid.$PLAYER_PARAMS.'" frameborder="0" allowfullscreen></iframe>';
	break;
}
?>
</div>
</div>
