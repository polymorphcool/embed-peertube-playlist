<?php if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly ?>
<script>

	jQuery(document).ready(function(){

		jQuery('#playlist_peertube_grid_<?= $playlist->id ?> .video .video_container').click(function(){

			//supprime la vidéo déjà en cours de lecture
			jQuery('#playlist_peertube_grid_<?= $playlist->id ?> .video .video_container iframe').remove();

			//affiche le lecteur youtube
			jQuery(this).append('<iframe width="100%" height="100%" src="'+jQuery(this).attr('rel')+'?autoplay=1&rel=0" frameborder="0" allowfullscreen></iframe>');

		});

	})
	

</script>
<div class="playlist_peertube_grid" id="playlist_peertube_grid_<?= $playlist->id ?>">
<?php

foreach($data->data as $video)
{
		echo '<div class="video">';
		if($playlist->show_title)
			echo '<h3 style="color: '.$playlist->text_color.'; font-size: '.$playlist->text_size.'px;">'.$video->video->name.'</h3>';
		echo '<div class="video_container" rel="'.$peertube_url.$video->video->embedPath.'">';
		echo  '<img class="thumbnail" src="'.$peertube_url.$video->video->previewPath.'" />';
		if($playlist->show_description == 1)
			echo '<span class="video_description">'.$video->video->description.'</span>';
		echo '<img class="play_video" src="'.plugins_url( 'embed-peertube-playlist/images/logo.svg').'" />';
		echo '</div></div>';
}

?>
</div>