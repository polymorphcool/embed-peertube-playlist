<?php if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly ?>
<div class="wrap">
<h2>Peertube Playlists settings</h2>

<form method="post" action="">
    <?php wp_nonce_field( 'pl_pt_settings' ); ?>
    
    <table class="form-table">
        <tr valign="top">
            <th scope="row">Peertube instance url:</th>
            <td><input type="text" name="pl_pt_url" value="<?php echo esc_attr( get_option('pl_pt_url') ); ?>" required />
            </td>
        </tr>
    </table>
    
    <?php submit_button(); ?>

</form>
</div>