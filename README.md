 # Embed Peertube Playlist

Contributors: manu225
Donate link: http://www.info-d-74.com
Tags: playlist, peertube, video, grid, slider, list, peertube playlist, playlist peertube, peertube plugin, video playlist, peertube player, peertube integration, peertube api, player video, video integration, peertube embed, video embed
Requires at least: 3.5
Tested up to: 5.4
Stable tag: 1.03
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Display peertube playlist on your webiste.

## Description

Display peertube playlist on your webiste. [Here some exemples](https://www.info-d-74.com/peertube-playlist-demo/)

## Installation

1. Upload the plugin files to the `/wp-content/plugins/` directory, or install the plugin through the WordPress plugins screen directly.
2. Activate the plugin through the 'Plugins' screen in WordPress
3. Go Playlists peertube to create your first playlist


## Frequently Asked Questions

1. The plugin work with any playlist peertube?

No, only with playlists that are public

## Screenshots

1. assets/screenshot.png

2. views/player.php on homepage

![](https://gitlab.com/polymorphcool/embed-peertube-playlist/-/raw/master/screenshots/homepage_playlist.png)

## Changelog

2020.05.22 : frankiezafe added *views/player.php* that generates a videoplayer and a menu with links to playlist videos